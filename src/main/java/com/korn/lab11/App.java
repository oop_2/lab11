package com.korn.lab11;

public class App 
{
    public static void main( String[] args )
    {
        Rat rat1 = new Rat("Rattata", 4);
        rat1.eat();
        rat1.walk();
        rat1.run();
        rat1.sleep();

        Human human1 = new Human("korn", 2);
        human1.eat();
        human1.walk();
        human1.run();
        human1.sleep();

        Dog dog1  = new Dog("Growlithe", 4);
        dog1.eat();
        dog1.walk();
        dog1.run();
        dog1.sleep();

        Cat cat1 = new Cat("Meowth", 4);
        cat1.eat();
        cat1.walk();
        cat1.run();
        cat1.sleep();

        Bird bird1 = new Bird("Pidgey", 2);
        bird1.fly();
        bird1.takeoff();
        bird1.landing();
        bird1.eat();
        bird1.sleep();

        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.fly();
        bat1.landing();
        bat1.takeoff();

        Plane plane1 = new Plane("Boing", "Boing Engine");
        plane1.takeoff();
        plane1.landing();
        plane1.fly();

        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        Submarine submarine1 = new Submarine("tu", "tupower");
        submarine1.swim();

        Snake snake1 = new Snake("Ekans", 0);
        snake1.crawl();
        snake1.eat();
        snake1.sleep();

        Crocodile crocodile1 = new Crocodile("Sandile", 4);
        crocodile1.crawl();
        crocodile1.eat();
        crocodile1.swim();
        crocodile1.sleep();
    }
}
