package com.korn.lab11;

public interface Flyable {
    public void fly();
    public void landing();
    public void takeoff();
}
