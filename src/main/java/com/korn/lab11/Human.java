package com.korn.lab11;

public class Human extends Animal implements Walkable{

    public Human(String name, int numberOfLeg) {
        super(name, 2);
    }

    @Override
    public void walk() {
        System.out.println(this + " Walk.");
    }

    @Override
    public void run() {
        System.out.println(this + " run.");
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");
    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
    }
    @Override
    public String toString() {
        return "Human(" + this.getName() + ")";
    }
    
}
